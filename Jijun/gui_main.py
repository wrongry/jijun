#
# Jijun - Visually adjust time on sets of photos.
#
# Copyright (C) 2017      Daniel Henley <daniel.henley@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import datetime
import subprocess
import sys
from pkg_resources import resource_filename

from PyQt5 import uic
from PyQt5.QtCore import (QDir,
                          Qt)
from PyQt5.QtGui import (QBrush,
                         QColor,
                         QPixmap)
from PyQt5.QtWidgets import (QAbstractItemView,
                             QApplication,
                             QFileDialog,
                             QListWidgetItem,
                             QMainWindow,
                             QMenu,
                             QMessageBox)

# import application metadata
from Jijun import meta


CONST_LEFT = "left"
CONST_RIGHT = "right"
CONST_COLOUR_LEFT = QBrush(QColor(226, 190, 255))
CONST_COLOUR_RIGHT = QBrush(QColor(219, 248, 218))
CONST_COLOUR_PINNED = QBrush(QColor(214, 28, 28))


class JijunGui(QMainWindow):
    def __init__(self, **kwds):
        def gui_setup_not_handled_by_designer():
            self.le_left_directory.setStyleSheet(
                "background-color: rgb(226, 190, 255)")
            self.le_right_directory.setStyleSheet(
                "background-color: rgb(219, 248, 218)")
            # self.lw_files doesn't get drag/drop events, it leaves that up to
            # the viewport.  Who would have thought...
            self.lw_files.model().rowsMoved.connect(self.onRowsMoved)
            self.lw_files.customContextMenuRequested.connect(
                self.onContextMenu)

        self.app = QApplication(sys.argv)
        super().__init__(**kwds)
        ui = os.path.join(meta.DATA_ROOT, 'ui', 'jijun.ui')
        uic.loadUi(ui, self)
        self._last_left_directory = None
        self._last_right_directory = None
        self.left_file_list = []
        self.left_date_times = []
        self.right_file_list = []
        self.right_date_times = []
        self._pinned_item = None
        gui_setup_not_handled_by_designer()
        self.show()

    def onRowsMoved(self, sidx, sstart, send, didx, drow):
        # Rows are moving down the list
        if sstart < drow:
            widget_adj = self.lw_files.item(drow - 1)
            widget_ref = self.lw_files.item(drow - 2)
            (f, side) = widget_adj.data(Qt.UserRole)
            (f_alt, side_alt) = widget_ref.data(Qt.UserRole)
            if side_alt == side:
                widget_ref = self.lw_files.item(drow)
            self._adjust_widget_time(widget_adj, widget_ref, 1)
        # Rows are moving up the list
        elif sstart > drow:
            widget_adj = self.lw_files.item(drow)
            widget_ref = self.lw_files.item(drow + 1)
            (f, side) = widget_adj.data(Qt.UserRole)
            (f_alt, side_alt) = widget_ref.data(Qt.UserRole)
            if side_alt == side:
                widget_ref = self.lw_files.item(drow - 1)
            self._adjust_widget_time(widget_adj, widget_ref, -1)

    def onContextMenu(self, point):
        item = self.lw_files.itemAt(point)
        if item == None:
            return
        (fname, _) = item.data(Qt.UserRole)
        cm = QMenu(self)
        act_pin = cm.addAction("Pin as bottom image")
        act_unpin = cm.addAction("Unpin bottom image")
        act_before = cm.addAction("Move before pinned")
        act_after = cm.addAction("Move after pinned")
        if self._pinned_item is not None:
            act_pin.setEnabled(False)
        else:
            act_unpin.setEnabled(False)
            act_before.setEnabled(False)
            act_after.setEnabled(False)
        action = cm.exec_(self.lw_files.mapToGlobal(point))

        if action == act_pin:
            self._pin_item(item)
        elif action == act_unpin:
            self._unpin_item()
        elif action == act_before:
            self._adjust_to_pinned(item, -1)
        elif action == act_after:
            self._adjust_to_pinned(item, 1)
        else:  # Context menu not clicked
            return

    def onActionTriggered(self):
        actn = self.sender().objectName()
        if actn == "actn_quit":
            self.app.quit()
        elif actn == "actn_about":
            self._show_about_dialog()

    def onButtonClicked(self):
        btn = self.sender().objectName()
        if btn == "pb_left_select_directory":
            self._select_directory(CONST_LEFT)
            self._update_display_file_list()
            self._update_checkbox_hide()
        elif btn == "pb_right_select_directory":
            self._select_directory(CONST_RIGHT)
            self._update_display_file_list()
            self._update_checkbox_hide()
        elif btn == "pb_left_apply_to_files":
            self._apply_to_files(CONST_LEFT)
        elif btn == "pb_right_apply_to_files":
            self._apply_to_files(CONST_RIGHT)
        else:
            print("Error: Unknown button clicked: %s" % (btn))

    def onCheckboxToggled(self, checked):
        self._update_checkbox_hide()

    def onSpinboxChanged(self, val):
        sb = self.sender().objectName()
        if sb == "sb_left_hours":
            self._set_valid_spinbox_ranges(val, CONST_LEFT)
        elif sb == "sb_left_minutes":
            self._set_valid_spinbox_ranges(val, CONST_LEFT)
        elif sb == "sb_left_seconds":
            self._set_valid_spinbox_ranges(val, CONST_LEFT)
        elif sb == "sb_right_hours":
            self._set_valid_spinbox_ranges(val, CONST_RIGHT)
        elif sb == "sb_right_minutes":
            self._set_valid_spinbox_ranges(val, CONST_RIGHT)
        elif sb == "sb_right_seconds":
            self._set_valid_spinbox_ranges(val, CONST_RIGHT)
        item = self.lw_files.currentItem()
        if item is not None:
            fname = item.text()[15:]
        else:
            fname = ""
        # Rebuild file list with new delta + timestamp
        self._update_display_file_list()
        self._update_checkbox_hide()
        # Find last file and reload images from cache if possible
        item = self._item_from_fname(fname)
        self.lw_files.setCurrentItem(item)
        self._display_images(item)

    def onListWidgetRowChanged(self, row):
        # Ignore selection on right clicks
        if self.app.mouseButtons() == Qt.RightButton:
            return
        if row == -1:
            self.lb_top_details.clear()
            self.lb_top_image.clear()
            self.lb_bottom_details.clear()
            self.lb_bottom_image.clear()
            return
        item = self.lw_files.item(row)
        self._display_images(item)

    def _adjust_widget_time(self, widget_adj, widget_ref, seconds):
        def get_time_diff(t1, t2):
            print("Times: t1(%s) t2(%s)" % (t1, t2))
            s = int(t2[4:6]) - int(t1[4:6]) + seconds
            m = int(t2[2:4]) - int(t1[2:4])
            if s > 59:
                m += 1
            h = int(t2[:2]) - int(t1[:2])
            if m > 59:
                h += 1
            return (h, m, s)
        (f, side) = widget_adj.data(Qt.UserRole)
        time_adj = widget_adj.text()[8:14]
        time_ref = widget_ref.text()[8:14]
        (h, m, s) = get_time_diff(time_adj, time_ref)
        if side == CONST_LEFT:
            print("Set left %d:%d:%d" % (h, m, s))
            self.sb_left_hours.setValue(h + self.sb_left_hours.value())
            self.sb_left_minutes.setValue(m + self.sb_left_minutes.value())
            self.sb_left_seconds.setValue(s + self.sb_left_seconds.value())
        if side == CONST_RIGHT:
            print("Set right %d:%d:%d" % (h, m, s))
            self.sb_right_hours.setValue(h + self.sb_right_hours.value())
            self.sb_right_minutes.setValue(m + self.sb_right_minutes.value())
            self.sb_right_seconds.setValue(s + self.sb_right_seconds.value())
        self._update_display_file_list()
        item = self._item_from_fname(f)
        self.lw_files.setCurrentItem(item)

    def _reselect_top_image(self):
        fname = os.path.basename(self.lb_top_details.text())
        item = self._item_from_fname(fname)
        self.lw_files.setCurrentItem(item)

    def _item_from_fname(self, fname):
        if fname == "":
            return None
        fname = os.path.basename(fname)
        item_list = self.lw_files.findItems(fname, Qt.MatchEndsWith)
        if len(item_list) > 0:
            return(item_list[0])
        else:
            return None

    def _pin_item(self, item):
        item.setBackground(CONST_COLOUR_PINNED)
        # Use a copy to avoid crashes when file list is changed
        self._pinned_item = QListWidgetItem(item)
        self._reselect_top_image()
        self._display_images(None)

    def _unpin_item(self):
        (fname, side) = self._pinned_item.data(Qt.UserRole)
        item = self._item_from_fname(fname)
        self._pinned_item = None
        if side == CONST_LEFT:
            item.setBackground(CONST_COLOUR_LEFT)
        elif side == CONST_RIGHT:
            item.setBackground(CONST_COLOUR_RIGHT)
        self._reselect_top_image()

    def _adjust_to_pinned(self, item, sec_offset):
        def repin_pinned():
            (fname, _) = self._pinned_item.data(Qt.UserRole)
            item = self._item_from_fname(fname)
            self._pin_item(item)
        (fname, _) = item.data(Qt.UserRole)
        self._adjust_widget_time(item, self._pinned_item, sec_offset)
        item = self._item_from_fname(fname)
        repin_pinned()
        self.lw_files.setCurrentItem(item)

    def _next_visible_item(self, item):
        idx = self.lw_files.indexFromItem(item)
        while True:
            idx = idx.sibling(idx.row() + 1, idx.column())
            i = self.lw_files.itemFromIndex(idx)
            if i is None:
                break
            if not i.isHidden():
                break
        return i

    def _display_images(self, top_item):
        def get_full_path(fname):
            if fname == "":
                return ""
            # Find original file name
            path = next((s for s in self.left_file_list if fname in s), None)
            if path is not None:
                return path
            path = next((s for s in self.right_file_list if fname in s), None)
            return path

        def display_image(fname, lbl_name, lbl_img, cache):
            path = get_full_path(fname)
            # Show image
            if path == "":
                lbl_name.clear()
                lbl_img.clear()
                return
            for c in cache:
                if fname == c[0]:
                    lbl_name.setText(c[0])
                    lbl_img.setPixmap(c[1])
                    return
            lbl_name.setText(path)
            img = QPixmap(path)
            img = img.scaled(lbl_img.width(),
                             lbl_img.height(),
                             Qt.KeepAspectRatio,
                             Qt.SmoothTransformation)
            lbl_img.setPixmap(img)

        if self._pinned_item:
            bottom_item = self._pinned_item
        else:
            bottom_item = self._next_visible_item(top_item)
        # Cache top fname and pixmap to allow shortcut loading in both
        # directions
        # Need to create new QPixmap, as the QLabel just loads the new QPixmap
        # into it's QPixmap pointer
        if top_item is None:
            top_fname = ""
        else:
            (top_fname, _) = top_item.data(Qt.UserRole)
        if bottom_item is None:
            bottom_fname = ""
        else:
            (bottom_fname, _) = bottom_item.data(Qt.UserRole)
        c_top_fname = self.lb_top_details.text()
        c_top_img = QPixmap(self.lb_top_image.pixmap())
        c_bottom_fname = self.lb_bottom_details.text()
        c_bottom_img = QPixmap(self.lb_bottom_image.pixmap())
        cache = [(c_top_fname, c_top_img), (c_bottom_fname, c_bottom_img)]
        display_image(top_fname,
                      self.lb_top_details,
                      self.lb_top_image,
                      cache)
        display_image(bottom_fname,
                      self.lb_bottom_details,
                      self.lb_bottom_image,
                      cache)

    def _select_directory(self, side):
        if side == CONST_LEFT:
            last_dir = self._last_left_directory
        elif side == CONST_RIGHT:
            last_dir = self._last_right_directory

        if last_dir is None:
            last_dir = QDir.currentPath()

        d = QFileDialog.getExistingDirectory(
                self,
                "Select Directory",
                last_dir,
                options=QFileDialog.ShowDirsOnly)
        if d == "":
            return
        last = QDir(d)
        last.cdUp()
        last_dir = last.absolutePath()

        if side == CONST_LEFT:
            self._last_left_directory = last_dir
            self.le_left_directory.setText(d)
            self.left_file_list = self._get_file_list(d)
            self.left_date_times = (
                self._get_files_date_time(self.left_file_list))
        elif side == CONST_RIGHT:
            self._last_right_directory = last_dir
            self.le_right_directory.setText(d)
            self.right_file_list = self._get_file_list(d)
            self.right_date_times = (
                self._get_files_date_time(self.right_file_list))

    def _update_display_file_list(self):
        def add_to_listwidget(files, datetimes, side):
            for f, d in zip(files, datetimes):
                fname = d.strftime("%Y%m%d%H%M%S") + "_" + os.path.basename(f)
                i = QListWidgetItem(fname)
                # Save custom data in the item
                # full_path, list_side
                i.setData(Qt.UserRole, (f, side))
                if side == CONST_LEFT:
                    i.setBackground(CONST_COLOUR_LEFT)
                elif side == CONST_RIGHT:
                    i.setBackground(CONST_COLOUR_RIGHT)
                self.lw_files.addItem(i)
        # Disable signals from self.lw_files so we don't reload all images
        # on every time delta change
        self.lw_files.blockSignals(True)
        self.lw_files.clear()
        # Update left file display names
        fl = self.left_file_list
        dt = self._add_delta(
            self.left_date_times,
            self._get_delta(CONST_LEFT))
        add_to_listwidget(fl, dt, CONST_LEFT)
        # Update right file display names
        fl = self.right_file_list
        dt = self._add_delta(
            self.right_date_times,
            self._get_delta(CONST_RIGHT))
        add_to_listwidget(fl, dt, CONST_RIGHT)
        self.lw_files.sortItems()
        # Re-enable self.lw_files signals
        self.lw_files.blockSignals(False)

    def _update_checkbox_hide(self):
        show_sides = []
        if self.cb_left_show_files.isChecked():
            show_sides.append(CONST_LEFT)
        if self.cb_right_show_files.isChecked():
            show_sides.append(CONST_RIGHT)

        new_item_set = False
        cur_item_found = False
        new_item = None
        current_item = self.lw_files.currentItem()
        for i in range(self.lw_files.count()):
            item = self.lw_files.item(i)
            (f, side) = item.data(Qt.UserRole)
            if side in show_sides:
                item.setHidden(False)
            else:
                item.setHidden(True)
            if not new_item_set:
                if item == current_item:
                    cur_item_found = True
                if not cur_item_found:
                    continue
                if not item.isHidden():
                    new_item_set = True
                    new_item = item

        if new_item is None:
            self.lw_files.setCurrentRow(-1)
        else:
            self.lw_files.setCurrentItem(new_item)
            self.lw_files.scrollToItem(new_item,
                                       QAbstractItemView.PositionAtCenter)

    def _set_valid_spinbox_ranges(self, val, side):
        if side == CONST_LEFT:
            h = self.sb_left_hours
            m = self.sb_left_minutes
            s = self.sb_left_seconds
        elif side == CONST_RIGHT:
            h = self.sb_right_hours
            m = self.sb_right_minutes
            s = self.sb_right_seconds

        if val > 0:
            h.setRange(0, 10)
            m.setRange(0, 59)
            s.setRange(0, 59)
        elif val < 0:
            h.setRange(-10, 0)
            m.setRange(-59, 0)
            s.setRange(-59, 0)
        else:
            if h.value() != 0 or m.value() != 0 or s.value() != 0:
                return
            h.setRange(-10, 10)
            m.setRange(-59, 59)
            s.setRange(-59, 59)

    def _apply_to_files(self, side):
        files = []
        h = 0
        m = 0
        s = 0
        if side == CONST_LEFT:
            files = self.left_file_list
            hmax = self.sb_left_hours.maximum()
            h = self.sb_left_hours.value()
            m = self.sb_left_minutes.value()
            s = self.sb_left_seconds.value()
        elif side == CONST_RIGHT:
            files = self.right_file_list
            hmax = self.sb_right_hours.maximum()
            h = self.sb_right_hours.value()
            m = self.sb_right_minutes.value()
            s = self.sb_right_seconds.value()

        if hmax > 0:
            arg = "-ta+"
        else:
            arg = "-ta-"
        cmd = "jhead -q %s%d:%d:%d %s" % (arg, abs(h), abs(m),
                                          abs(s), " ".join(files))
        print(cmd)
        sp = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        for line in sp.stdout.readlines():
            print(line)

    def _get_file_list(self, path):
        if path == "":
            return []
        qd = QDir(path)
        qd.setNameFilters(["*.jpg"])
        path = qd.absolutePath() + qd.separator()
        return [path + f for f in qd.entryList()]

    def _get_files_date_time(self, files):
        if files == []:
            return []
        times = []
        cmd = subprocess.Popen(["jhead"] + files, stdout=subprocess.PIPE)
        for line in cmd.stdout.readlines():
            s = line.decode().strip()
            if s.startswith("Date/Time"):
                strs = s.rsplit()
                dateStr = strs[-2] + ":" + strs[-1]
                dt = datetime.datetime.strptime(dateStr, "%Y:%m:%d:%H:%M:%S")
                times.append(dt)
        return times

    def _add_delta(self, datetimes, delta):
        times = []
        for dt in datetimes:
            times.append(dt + delta)
        return times

    def _get_delta(self, side):
        delta = datetime.timedelta()
        if side == CONST_LEFT:
            delta = datetime.timedelta(
                hours=self.sb_left_hours.value(),
                minutes=self.sb_left_minutes.value(),
                seconds=self.sb_left_seconds.value())
        elif side == CONST_RIGHT:
            delta = datetime.timedelta(
                hours=self.sb_right_hours.value(),
                minutes=self.sb_right_minutes.value(),
                seconds=self.sb_right_seconds.value())
        return delta

    def _show_about_dialog(self):
        QMessageBox.about(self,
                          "About %s" % (meta.NAME),
                          "Version %s"
                          "\n\n"
                          "%s is a graphical interface for\n"
                          "setting the exif timestamps in photos from\n"
                          "cameras with disparate clocks"
                          "\n\n"
                          "Copyright (C) 2017 Daniel Henley"
                          % (meta.VERSION, meta.NAME))


def exec():
    """
    Start Jijun's gui

    Simply import this module and execute this exec() function.
    """
    gui = JijunGui()
    gui.app.exec_()
