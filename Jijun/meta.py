#
# Jijun - Visually adjust time on sets of photos.
#
# Copyright (C) 2017      Daniel Henley <daniel.henley@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

VERSION_TUPLE = (0, 0, 1)
VERSION_QUALIFIER = ''
VERSION = '.'.join(map(str, VERSION_TUPLE)) + VERSION_QUALIFIER
major_version = '%s.%s' % (VERSION_TUPLE[0], VERSION_TUPLE[1])

AUTHOR = 'Daniel Henley'
AUTHOR_EMAIL = 'daniel.henley@gmail.com'
MAINTAINER = 'Daniel Henley'
MAINTAINER_EMAIL = 'daniel.henley@gmail.com'

NAME = 'Jijun'
DESCRIPTION = ('Jijun - Visually adjust timestamps on photos from different'
               'cameras')
URL = 'https://bitbucket.org/wrongry/jijun'
LICENSE = 'GPL v3'
PLATFORMS = ['Linux']

PKG_ROOT = os.path.abspath(os.path.dirname(__file__))
DATA_ROOT = os.path.join(PKG_ROOT, 'data')
