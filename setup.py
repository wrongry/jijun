#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Jijun - Visually adjust time on sets of photos.
#
# Copyright (C) 2017      Daniel Henley <daniel.henley@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''
Jijun distutils module.
'''

from Jijun import meta
import distutils.command.build_py
import sys

# check python version
if sys.version_info < (3, 2):
    raise SystemExit("Jijun requires Python 3.2 or later.")


packages = [meta.NAME]
PACKAGE_DIR = 'Jijun'

DOC_FILES = ['AUTHORS', 'COPYING']


distutils.core.setup(
    name=meta.NAME,
    description=meta.DESCRIPTION,
    version=meta.VERSION,
    author=meta.AUTHOR,
    author_email=meta.AUTHOR_EMAIL,
    maintainer=meta.MAINTAINER,
    maintainer_email=meta.MAINTAINER_EMAIL,
    url=meta.URL,
    license=meta.LICENSE,
    platforms=meta.PLATFORMS,
    packages=packages,
    package_dir={meta.NAME: PACKAGE_DIR},
    scripts=['jijun'],
    package_data={meta.NAME: ['data/ui/jijun.ui']},
    classifiers=[
        "Environment :: X11 Applications",
        "Environment :: X11 Applications :: Qt",
        "Intended Audience :: Other Audience",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3"
        ]
)
